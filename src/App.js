import React from 'react';
import Checkbox from './Component/Checkbox';
import Message from './Component/Message';
import Result from './Component/Result';
import SignUp from './Component/Signup';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fName: '',
      lName: '',
      date: '',
      email: '',
      address: '',
      message: '',
      radioOne: true,
      radioTwo: false,
      validFirstName: true,
      validLastName: true,
      validDate: true,
      validEmail: true,
      validAddress: true,
      validMessage: true,
      isSignupValid: false,
      isMessageValid: false,
      isCheckboxValid: false,
    };
  }

  validName = (input) => {
    if (input.length < 3) return false;
    if (input[0] === '') return false;
    return true;
  };
  validDate = (input) => {
    if (input === '') return false;
    let year = input.split('-')[0];
    if (year > 1980 && year < 2011) return true;
    return false;
  };
  validEmail = (input) => {
    if (input.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
      return true;
    } else {
      return false;
    }
  };

  handleSubmitStep = (value) => {
    const checkName = this.validName(this.state.fName);
    const checkLastName = this.validName(this.state.lName);
    const checkAddress = this.validName(this.state.address);
    const checkDate = this.validDate(this.state.date);
    const checkEmail = this.validEmail(this.state.email);
    if (!checkName) {
      this.setState({
        validFirstName: false,
      });
    } else {
      this.setState({
        validFirstName: true,
      });
    }
    if (!checkLastName) {
      this.setState({
        validLastName: false,
      });
    } else {
      this.setState({
        validLastName: true,
      });
    }
    if (!checkAddress) {
      this.setState({
        validAddress: false,
      });
    } else {
      this.setState({
        validAddress: true,
      });
    }
    if (!checkDate) {
      this.setState({
        validDate: false,
      });
    } else {
      this.setState({
        validDate: true,
      });
    }
    if (!checkEmail) {
      this.setState({
        validEmail: false,
      });
    } else {
      this.setState({
        validEmail: true,
      });
    }
    if (checkName && checkLastName && checkDate && checkEmail && checkAddress) {
      this.setState({
        isSignupValid: true,
      });
    } else {
      this.setState({
        isSignupValid: false,
      });
    }
  };
  handleBackAction = () => {
    this.setState({
      isSignupValid: false,
    });
  };

  handleMessageSubmit = () => {
    if (this.state.message !== '') {
      this.setState({
        isMessageValid: true,
        validMessage: true,
      });
    } else {
      this.setState({
        validMessage: false,
      });
    }
  };
  handleCheckboxBackButton = () => {
    this.setState({
      isMessageValid: false,
    });
  };
  handleChangeValue = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value,
    });
  };
  handleMessageRadio = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if (e.target.id === 'one') {
      this.setState({
        [name]: value,
        radioOne: true,
        radioTwo: false,
      });
    } else {
      this.setState({
        [name]: value,
        radioOne: false,
        radioTwo: true,
      });
    }
  };
  handleSubmit = () => {
    this.setState({
      isCheckboxValid: true,
    });
  };
  render() {
    const {
      fName,
      lName,
      date,
      email,
      address,
      message,
      validFirstName,
      isSignupValid,
      isMessageValid,
      isCheckboxValid,
      validLastName,
      validDate,
      validEmail,
      validAddress,
      validMessage,
    } = this.state;
    if (!isSignupValid) {
      return (
        <div className="App">
          <SignUp
            fName={fName}
            lName={lName}
            date={date}
            email={email}
            address={address}
            validFirstName={validFirstName}
            validLastName={validLastName}
            validAddress={validAddress}
            validDate={validDate}
            validEmail={validEmail}
            increaseCount={this.handleSubmitStep}
            changeValue={this.handleChangeValue}
          />
        </div>
      );
    } else if (isSignupValid && !isMessageValid) {
      return (
        <div className="App">
          <Message
            validMessage={validMessage}
            radioOne={this.state.radioOne}
            radioTwo={this.state.radioTwo}
            message={message}
            messageHistory={this.handleMessageSubmit}
            backToSignUp={this.handleBackAction}
            changeValue={this.handleChangeValue}
            radioChangeValue={this.handleMessageRadio}
          />
        </div>
      );
    } else if (isMessageValid && !isCheckboxValid) {
      return (
        <div className="App">
          <Checkbox
            validMessage={validMessage}
            backToMessage={this.handleCheckboxBackButton}
            submit={this.handleSubmit}
          />
        </div>
      );
    } else {
      return (
        <div className="App">
          <Result
            fName={fName}
            lName={lName}
            date={date}
            email={email}
            address={address}
          />
        </div>
      );
    }
  }
}

export default App;
