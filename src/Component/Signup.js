import React from 'react';
import image from '../Images/signUp.png';

class SignUp extends React.Component {
  render() {
    const { count, fName, lName, date, email, address } = this.props;
    return (
      <div className="SignUp">
        <div className="imageDiv">
          <img src={image} alt="signupImage" />
        </div>

        <div className="aside">
          <div className="Navbar">
            <div className="nav-container">
              <div className="one check">1</div>
              <div className="text">Signup</div>
            </div>
            <div className="nav-container">
              <div className="two check">2</div>
              <div className="text">Message</div>
            </div>
            <div className="nav-container">
              <div className="three check">3</div>
              <div className="text">Checkbox</div>
            </div>
          </div>
          <div className="main-content">
            <p>Step1/3</p>
            <h3>Sign UP</h3>
            <div className="name">
              <div className="first-block">
                <lable for="firstName">First Name</lable>
                <br />
                <input
                  type="text"
                  name="fName"
                  className="first-name text-place"
                  placeholder="Enter first Name"
                  value={fName}
                  onChange={(e) => {
                    this.props.changeValue(e);
                  }}
                  required
                />
                <br />
                <p
                  style={{
                    display: this.props.validFirstName ? 'none' : 'contents',
                    color: 'red',
                    fontSize: '10px',
                  }}
                >
                  *Name conatains atleast 3 letter
                </p>
              </div>
              <div>
                <lable for="lastName">Last Name</lable>
                <br />
                <input
                  type="text"
                  className="lastName text-place"
                  name="lName"
                  value={lName}
                  placeholder="Last name"
                  onChange={(e) => {
                    this.props.changeValue(e);
                  }}
                  required
                />
                <br />
                <p
                  style={{
                    display: this.props.validLastName ? 'none' : 'contents',
                    color: 'red',
                    fontSize: '10px',
                  }}
                >
                  *Please enter your last name
                </p>
              </div>
            </div>

            <div className="other">
              <div className="second-block">
                <lable for="date">Date of birth</lable>
                <br />
                <input
                  type="date"
                  name="date"
                  min="1980-12-01"
                  max="2010-12-01"
                  value={date}
                  className="text-place"
                  onChange={(e) => {
                    this.props.changeValue(e);
                  }}
                  required
                />
                <br />
                <p
                  style={{
                    display: this.props.validDate ? 'none' : 'contents',
                    color: 'red',
                    fontSize: '10px',
                  }}
                >
                  {' '}
                  *Please enter your D.O.B
                </p>
              </div>
              <div>
                <lable for="email">Email Address</lable>
                <br />
                <input
                  type="email"
                  className="email text-place"
                  name="email"
                  value={email}
                  placeholder="abc@gmail.com"
                  onChange={(e) => {
                    this.props.changeValue(e);
                  }}
                  required
                />
                <br />
                <p
                  style={{
                    display: this.props.validEmail ? 'none' : 'contents',
                    color: 'red',
                    fontSize: '10px',
                  }}
                >
                  *Please enter your correct email address
                </p>
              </div>
            </div>
            <div className="address">
              <lable for="address">Address</lable>
              <br />
              <input
                type="text"
                className="  addr-text-place"
                name="address"
                value={address}
                placeholder="Enter Your Address"
                onChange={(e) => {
                  this.props.changeValue(e);
                }}
                required
              />
              <br />
              <p
                style={{
                  display: this.props.validAddress ? 'none' : 'contents',
                  color: 'red',
                  fontSize: '10px',
                }}
              >
                *Please enter your Address
              </p>
            </div>
          </div>
          <button
            className="btn"
            onClick={() => this.props.increaseCount(count)}
          >
            Next step
          </button>
        </div>
      </div>
    );
  }
}

export default SignUp;
