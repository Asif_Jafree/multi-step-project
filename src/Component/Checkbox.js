import React from 'react';
import image from '../Images/checkbox.png';
import girl from '../Images/girllogo.png';
import boy from '../Images/boylogo.png';
class Checkbox extends React.Component {
  render() {
    return (
     
        <div className="SignUp">
          <div className="imageDiv">
            <img src={image} alt="signupImage" />
          </div>

          <div className="aside">
            <div className="Navbar">
              <div className="nav-container">
                <div
                  className="one check"
                  style={{
                    backgroundColor: '#AAA',
                    color: '#1776fa',
                    fontSize: '30px',
                  }}
                >
                  &#x2713;
                </div>
                <div className="text">Signup</div>
              </div>
              <div className="nav-container">
                <div
                  className="two check"
                  style={{
                    backgroundColor: '#AAA',
                    color: '#1776fa',
                    fontSize: '30px',
                  }}
                >
                  &#x2713;
                </div>
                <div className="text">Message</div>
              </div>
              <div className="nav-container">
                <div className="three check">3</div>
                <div className="text">Checkbox</div>
              </div>
            </div>
            <div className="main-content">
              <p>Step3/3</p>
              <h3>Checkbox</h3>
              <div className="checkbox">
                <label>
                  <input
                    type="radio"
                    name="test"
                    value="boy"
                    style={{
                      position: 'absolute',
                      opacity: 0,
                      width: 0,
                      height: 0,
                    }}
                    checked
                  />
                  <img src={boy} alt="" />
                </label>

                <label>
                  <input
                    type="radio"
                    name="test"
                    value="girl"
                    style={{
                      position: 'absolute',
                      opacity: 0,
                      width: 0,
                      height: 0,
                    }}
                  />
                  <img src={girl} alt="" />
                </label>
              </div>

              <div className="checkbox-option">
                <div className="checkbox-container">
                  <input type="checkbox" name="checkbox" />
                  <div className="checkbox-text">I want add this option</div>
                </div>
                <div className="checkbox-container">
                  <input type="checkbox" name="checkbox" />
                  <div className="checkbox-text">
                    Let me click on this checkbox and choose some cool stuf
                  </div>
                </div>
              </div>
            </div>
            <button className="btn" onClick={() => this.props.submit()}>
              Submit
            </button>
            <button
              className="btn"
              style={{ backgroundColor: '#fff', color: '#000' }}
              onClick={() => this.props.backToMessage()}
            >
              Back
            </button>
          </div>
        </div>
     
    );
  }
}

export default Checkbox;
