import React from 'react';
import image from '../Images/message.png';

class Message extends React.Component {
  render() {
    return (
      <div className="SignUp">
        <div className="imageDiv">
          <img src={image} alt="signupImage" />
        </div>

        <div className="aside">
          <div className="Navbar">
            <div className="nav-container">
              <div
                className="one check"
                style={{
                  backgroundColor: '#AAA',
                  color: '#1776fa',
                  fontSize: '30px',
                }}
              >
                &#x2713;
              </div>
              <div className="text">Signup</div>
            </div>
            <div className="nav-container">
              <div className="two check">2</div>
              <div className="text">Message</div>
            </div>
            <div className="nav-container">
              <div className="three check">3</div>
              <div className="text">Checkbox</div>
            </div>
          </div>
          <div className="main-content">
            <p>Step2/3</p>
            <h3>Message</h3>

            <div className="message">
              <p>Message</p>
              <textarea
                name="message"
                className="textArea"
                rows="7"
                cols="50"
                value={this.props.message}
                onChange={(e) => {
                  this.props.changeValue(e);
                }}
                required
              ></textarea>
              <br />
              <p
                style={{
                  display: this.props.validMessage ? 'none' : 'contents',
                  color: 'red',
                  fontSize: '10px',
                }}
              >
                Please enter your message first
              </p>
            </div>
            <div className="options">
              <input
                type="radio"
                name="choice"
                id="one"
                value="The number one choice"
                checked={this.props.radioOne}
                onChange={(e) => this.props.radioChangeValue(e)}
              />
              <lable for="one">The number one choice</lable>
              <br />
              <input
                type="radio"
                name="choice"
                id="two"
                value="The number two choice"
                checked={this.props.radioTwo}
                onChange={(e) => this.props.radioChangeValue(e)}
              />
              <lable for="two">The number two choice</lable>
            </div>
          </div>
          <button className="btn" onClick={() => this.props.messageHistory()}>
            Next step
          </button>
          <button
            className="btn"
            style={{ backgroundColor: '#fff', color: '#000' }}
            onClick={() => this.props.backToSignUp()}
          >
            Back
          </button>
        </div>
      </div>
    );
  }
}

export default Message;
