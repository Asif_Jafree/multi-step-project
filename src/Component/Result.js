import React from 'react';

class Result extends React.Component {
  render() {
    const { fName, lName, date, email, address } = this.props;
    return (
      <div className="result">
        <h1>Thank you</h1>
        <h2>Your Form Submitted Successfully</h2>
        <p>
          Name :<br />
          {fName} {lName}
        </p>
        <br />
        <p>
          Date of Birth:
          <br />
          {date}
        </p>
        <br />
        <p>
          Email :<br />
          {email}
        </p>
        <br />
        <p>
          Address :<br />
          {address}
        </p>
      </div>
    );
  }
}

export default Result;
